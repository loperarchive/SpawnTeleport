package org.nocraft.loperd.spawnteleport;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;

public enum Message {
    TOO_MANY_ARGUMENTS("&c&lFAILURE&f: &fДанная команда может содержать аргументов.");

    private final String message;

    Message(String message) {
        this.message = message;
    }

    public BaseComponent[] asComponent() {
        return TextComponent.fromLegacyText(format(this.message));
    }

    public BaseComponent[] asComponent(Object... objects) {
        return TextComponent.fromLegacyText(format(this.message, objects));
    }

    private static String format(String s, Object... objects) {
        for (int i = 0; i < objects.length; i++) {
            Object o = objects[i];
            s = s.replace("{" + i + "}", String.valueOf(o));
        }
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}
